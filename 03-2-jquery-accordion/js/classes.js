﻿function hasClass(node, className) {
  var classStr = node.className;
  
  if (classStr) {
    classStr = classStr.split(' ');
    
    if (classStr.indexOf(className) >= 0) return true;
  }
  
  return false;
}

function addClass(node, className) {
  className = className.split(' ');
  
  for (var i = 0; i < className.length; i++) {
    if (!hasClass(node, className[i])) {
      node.className += ' ' + className[i];
    }
  }
}

function removeClass(node, className) {
  className = className.split(' ');
  
  for (var i = 0; i < className.length; i++) {
    if (hasClass(node, className[i])) {
      var classes = node.className.split(' ');
      classes.splice(classes.indexOf(className[i]), 1);
      node.className = classes.join(' ');
    }
  }
}

function toggleClass(node, className) {
  className = className.split(' ');
  
  for (var i = 0; i < className.length; i++) {
    (hasClass(node, className[i])) ? removeClass(node, className[i]) : addClass(node, className[i]);
  }
}